# Changelog

## 1.0.0

- Upgrade dependencies, fix all hints.

## 0.0.5

- Upgrade dependencies.

## 0.0.4

- Make use of the `pub_client` library to read the latest version.

## 0.0.3

- Add analysis_options and make sure to await autoPublish.

## 0.0.2

- Fix issue when optional env vars haven't been set.

## 0.0.1

- Initial Release