# Contributing

Please create an issue before you start working on a feature,
and discuss your intent.

Make sure that all tests pass before you create a
Merge Request: `grind test`