part of '../grind_publish.dart';

@visibleForTesting
Map<String, String>? testEnvironment;

/// The Credentials used to authenticate to pub.
/// You will find them on your machine in `~/.pub-cache/credentials.json`
class Credentials {
  static const _defaultTokenEndpoint =
      'https://accounts.google.com/o/oauth2/token';
  static const _defaultScopes = [
    'openid',
    'https://www.googleapis.com/auth/userinfo.email'
  ];

  final String? accessToken;
  final String? refreshToken;

  /// Timestamp in milliseconds
  final int expiration;
  final String tokenEndpoint;
  final List<String>? scopes;

  Credentials({
    required this.accessToken,
    required this.refreshToken,
    required this.expiration,
    this.tokenEndpoint = _defaultTokenEndpoint,
    this.scopes = _defaultScopes,
  });

  /// Creates the credentials by reading the variables from the environment
  /// `TOKEN_ENDPOINT` and `SCOPES` are optional.
  factory Credentials.fromEnvironment({
    String accessTokenVarName = 'ACCESS_TOKEN',
    String refreshTokenVarName = 'REFRESH_TOKEN',
    String expirationVarName = 'EXPIRATION',
    String tokenEndpointVarName = 'TOKEN_ENDPOINT',
    String scopesVarName = 'SCOPES',
  }) {
    final environment = testEnvironment ?? Platform.environment;
    final scopes = environment[scopesVarName];
    return Credentials(
        accessToken: environment[accessTokenVarName],
        refreshToken: environment[refreshTokenVarName],
        expiration: int.parse(environment[expirationVarName]!),
        tokenEndpoint:
            environment[tokenEndpointVarName] ?? _defaultTokenEndpoint,
        scopes: scopes == null
            ? _defaultScopes
            : (jsonDecode(scopes) as List?)?.cast<String>());
  }

  String toJson() => jsonEncode({
        'accessToken': accessToken,
        'refreshToken': refreshToken,
        'expiration': expiration,
        'tokenEndpoint': tokenEndpoint,
        'scopes': scopes,
      });
}
