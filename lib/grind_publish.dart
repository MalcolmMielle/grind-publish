import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:grinder/grinder.dart';
import 'package:meta/meta.dart';
import 'package:path/path.dart' as path;
import 'package:pub_api_client/pub_api_client.dart';
import 'package:yaml/yaml.dart';

part 'src/credentials.dart';

/// Checks whether the currently published version is older than this version,
/// and automatically publishes it.
Future<Null> autoPublish(
  final String packageName,
  final Credentials credentials,
) async {
  if (!await _shouldPublish(packageName)) {
    log('Not publishing.');
    return;
  }

  log('Writing the credentials file...');

  final pubCacheDir =
      Directory(path.join(Platform.environment['HOME']!, '.pub-cache'));

  // Create the pub-cache directory for the key.
  await pubCacheDir.create(recursive: true);

  // Write the credentials file.
  await File(path.join(pubCacheDir.path, 'credentials.json'))
      .writeAsString(credentials.toJson());

  // Publish the package.
  run('pub', arguments: ['publish', '-f']);
}

Future<bool> _shouldPublish(String packageName) async {
  log('Comparing versions with pub now, to see if we should publish...');

  // Get the latest version for this package.
  final package = await PubClient().packageInfo(packageName);
  final publishedVersion = package.latest.version;

  final pubspec = await File('pubspec.yaml').readAsString();
  final version = loadYaml(pubspec)['version'];

  if (version != publishedVersion) {
    log('Publish the package! '
        'This version is $version and pub has $publishedVersion');
    return true;
  } else {
    log('Do not publish. Both versions are $version.');
    return false;
  }
}
